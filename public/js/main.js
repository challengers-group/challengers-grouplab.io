async function main() {
	let voronoiNoise = buildVoronoi(256, document.getElementById("voronoi"));
	let voronoiNoise2 = buildVoronoi(256, document.getElementById("voronoi2"), 131.0);
	let perlinNoise = buildPerlin(256, document.getElementById("perlin"));

	let terrainNoises = [voronoiNoise, voronoiNoise2, perlinNoise];
	let terrainNoiseScales = [0.05, 0.5, 1.0];
	let terrainNoisePowers = [0.25, 0.025, 0.0025];

	let objectNoises = [perlinNoise];
	let objectNoiseScales = [0.1];
	let objectNoisePowers = [1.0];

	let screen = document.getElementById("screen");

	let scene = new THREE.Scene();

	let camera = new THREE.PerspectiveCamera( 75, screen.width/screen.height, 0.01, 1000 );
	camera.position.set(1, 0, 3);
	camera.up = new THREE.Vector3(0,0,1);
	camera.lookAt(new THREE.Vector3(0,0,0));

	let renderer = new THREE.WebGLRenderer({
		antialias:true,
		canvas: screen
	});
	renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = THREE.BasicShadowMap;
	renderer.setClearColor("#ffb892");

	let square = 10;

	let centerX = 0;
	let centerY = 0;
	let meshesOnScene = {};

	let texture = loadTexture( "normal" );
	texture.wrapS = THREE.RepeatWrapping;
	texture.wrapT = THREE.RepeatWrapping;
	texture.repeat.set( 20, 20 );

	let terrainMaterial = new THREE.MeshPhongMaterial({
		//map: texture,
		//normalMap: texture,
		color: "#484328",
		flatShading: true
	});

	let cactusTemplate = await loadModel("Cactus_1");
	cactusTemplate.rotateX(Math.PI/2);
	cactusTemplate.scale.set(0.002, 0.002, 0.002);
	let objectTemplates = [cactusTemplate];

	let ambientLight = new THREE.AmbientLight("#ffaebe", 0.02);
	let hemisphereLight = new THREE.HemisphereLight("#ff802b", "#59daff", 0.5);
	scene.add(ambientLight);
	scene.add(hemisphereLight);

	let directionalLightTarget = new THREE.Object3D();
	directionalLightTarget.position.set(0, 0, 0);
	scene.add(directionalLightTarget);
	let directionalLight = new THREE.DirectionalLight("#ff7221", 1.0);
	directionalLight.castShadow = true;
	directionalLight.position.set(square, square, square/2);
	directionalLight.target = directionalLightTarget;
	directionalLight.shadow.mapSize.width = 4096;
	directionalLight.shadow.mapSize.height = 4096;
	scene.add(directionalLight);

	buildTerrain(terrainNoises, terrainNoiseScales, terrainNoisePowers, terrainMaterial,
			 	 objectNoises, objectNoiseScales, objectNoisePowers, objectTemplates,
				 centerX, centerY, square, meshesOnScene, scene);

	let mapControl = new THREE.MapControls(camera, screen);

	let render = function () {
		let actualCenterX = Math.floor(camera.position.x);
		let actualCenterY = Math.floor(camera.position.y);

		if(actualCenterX != centerX || actualCenterY != centerY) {
			buildTerrain(terrainNoises, terrainNoiseScales, terrainNoisePowers, terrainMaterial,
						 objectNoises, objectNoiseScales, objectNoisePowers, objectTemplates,
						 centerX, centerY, square, meshesOnScene, scene);
			centerX = actualCenterX;
			centerY = actualCenterY;

			directionalLightTarget.position.set(centerX, centerY, 0);
			directionalLight.position.set(centerX + square, centerY + square, square/2);
		}

		renderer.render(scene, camera);
		requestAnimationFrame( render );
	};
	render();
}

function loadTexture(name) {
	let textureLoader = new THREE.TextureLoader();
	return textureLoader.load( "pic/" + name + ".bmp" );
}

async function loadModel(name) {
	let materialLoader = new THREE.MTLLoader();
	let materialPromise = new Promise(async function (resolve, reject) {
		materialLoader.load(
			"model/" + name + ".mtl",
			function(material) {
				resolve(material);
			},
			undefined,
			function (error) {
				reject(error);
			});
	});

	let modelLoader = new THREE.OBJLoader();
	modelLoader.setMaterials(await materialPromise);
	let modelPromise = new Promise(async function (resolve, reject) {
		modelLoader.load(
			"model/" + name + ".obj",
			function(model) {
				model.traverse( function ( child ) {
					if(child instanceof THREE.Mesh) {
						child.receiveShadow = true;
						child.castShadow = true;
					}
				});
				resolve(model);
			},
			undefined,
			function (error) {
				reject(error);
			});
	});
	return await modelPromise;
}

function calculateZ(noises, scales, powers, x, y) {
	let z = 0;
	for(let k = 0; k < noises.length; k++) {
		z += noises[k].getRedNormal(x*scales[k], y*scales[k])*powers[k];
	}
	return z;
}

function buildNoiseTerrain(noises, scales, powers, xPos, yPos) {
	let terrainGeometry = new THREE.PlaneBufferGeometry( 1, 1, 10, 10 );
	let points = terrainGeometry.getAttribute("position");
	let hVerts = terrainGeometry.parameters.heightSegments + 1;
	let wVerts = terrainGeometry.parameters.widthSegments + 1;
	for (let j = 0; j < hVerts; j++) {
		for (let i = 0; i < wVerts; i++) {
			//+0 is x, +1 is y.
			let x = points.array[3*(j*wVerts+i)] + xPos;
			let y = points.array[3*(j*wVerts+i)+1] + yPos;
			points.array[3*(j*wVerts+i)+2] = calculateZ(noises, scales, powers, x, y);
		}
	}
	points.needsUpdate = true;
	terrainGeometry.computeVertexNormals();
	return terrainGeometry;
}

function buildTile(terrainNoises, terrainNoiseScales, terrainNoisePowers, terrainMaterial,
				   objectNoises, objectNoiseScales, objectNoisePowers, objectTemplates,
				   x, y) {
	let meshes = [];

	let terrainGeometry = buildNoiseTerrain(terrainNoises, terrainNoiseScales, terrainNoisePowers, x, y);
	let terrain = new THREE.Mesh( terrainGeometry, terrainMaterial );
	terrain.translateX(x);
	terrain.translateY(y);
	terrain.receiveShadow = true;
	terrain.castShadow = true;
	meshes.push(terrain);

	let agreementThreshold = 0.9;
	let agreement = calculateZ(objectNoises, objectNoiseScales, objectNoisePowers, x, y);
	if(agreement >= agreementThreshold) {
		let actualX = x + agreement;
		let actualY = y - agreement;
		let z = calculateZ(terrainNoises, terrainNoiseScales, terrainNoisePowers, actualX, actualY);
		let cactus = objectTemplates[0].clone();
		cactus.position.set(actualX, actualY, z);
		cactus.rotateY(Math.PI * (agreement - agreementThreshold)/(1 - agreementThreshold));
		meshes.push(cactus);
	}

	return meshes;
}

let skipTerrainBuild = false;
function buildTerrain(terrainNoises, terrainNoiseScales, terrainNoisePowers, terrainMaterial,
					  objectNoises, objectNoiseScales, objectNoisePowers, objectTemplates,
					  centerX, centerY, square, meshesOnScene, scene) {

	if(!skipTerrainBuild) {
		skipTerrainBuild = true;

		let t0 = performance.now();

		for (let xMesh in meshesOnScene) {
			if (Math.abs(xMesh - centerX) > square) {
				for (let yMesh in meshesOnScene[xMesh]) {
					//console.log("Purging (", xMesh, ";", yMesh, ")");
					for (let mesh of meshesOnScene[xMesh][yMesh]) {
						scene.remove(mesh);
					}
					//console.log("Purged (", xMesh, ";", yMesh, ")");
				}
				delete(meshesOnScene[xMesh]);
			}
		}

		for (let xMesh in meshesOnScene) {
			for (let yMesh in meshesOnScene[xMesh]) {
				if (Math.abs(yMesh - centerY) > square) {
					//console.log("Purging (", xMesh, ";", yMesh, ")");
					for (let mesh of meshesOnScene[xMesh][yMesh]) {
						scene.remove(mesh);
					}
					delete(meshesOnScene[xMesh][yMesh]);
					//console.log("Purged (", xMesh, ";", yMesh, ")");
				}
			}
		}

		for (let y = centerY - square; y <= centerY + square; y++) {
			for (let x = centerX - square; x <= centerX + square; x++) {
				if (!meshesOnScene[x]) {
					meshesOnScene[x] = {};
				}

				if (!meshesOnScene[x][y]) {
					let meshes = buildTile(terrainNoises, terrainNoiseScales, terrainNoisePowers, terrainMaterial,
						objectNoises, objectNoiseScales, objectNoisePowers, objectTemplates,
						x, y);
					for (let mesh of meshes) {
						scene.add(mesh);
					}
					meshesOnScene[x][y] = meshes;
				}
			}
		}

		let t1 = performance.now();
		console.log("Call to generate tile took " + (t1 - t0) + " milliseconds.");

		skipTerrainBuild = false;
	}
}