function buildVoronoi(resolution, canvas, seed) {
    let voronoiScene = new THREE.Scene();

    let voronoiCamera = new THREE.OrthographicCamera( -0.5, 0.5, 0.5, -0.5, 1, 1000 );
    voronoiCamera.position.z = 1;

    let voronoiRenderer = new THREE.WebGLRenderer({antialias:true, canvas: canvas});
    voronoiRenderer.setClearColor("#000000");

    let voronoiGeometry = new THREE.PlaneGeometry(1, 1, 1, 1);
    let voronoiMaterial =  new THREE.ShaderMaterial({
        uniforms: {
            u_Resolution: {type: "vec2", value: new THREE.Vector2(resolution, resolution)},
            u_Scale: {type: "float", value: 5.0},
            u_Seed: {type: "float", value: seed || 47.0}
        },
        fragmentShader: document.getElementById("voronoi-fragment").innerText,
        vertexShader: document.getElementById("simple-vertex").innerText
    });
    let voronoiMesh = new THREE.Mesh(voronoiGeometry, voronoiMaterial);
    voronoiScene.add( voronoiMesh );
    if(canvas) {
        voronoiRenderer.render(voronoiScene, voronoiCamera);
    }

    let renderTarget = new THREE.WebGLRenderTarget(resolution, resolution);
    voronoiRenderer.setRenderTarget(renderTarget);
    voronoiRenderer.render(voronoiScene, voronoiCamera);

    let buffer = new Uint8Array(resolution * resolution * 4);
    voronoiRenderer.readRenderTargetPixels(renderTarget, 0, 0, resolution, resolution, buffer);

    return new MatrixRGBA(buffer, resolution);
}