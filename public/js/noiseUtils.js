function MatrixRGBA(buffer, resolution) {
    this.buffer = buffer;
    this.resolution = resolution;
}

MatrixRGBA.prototype.constructor = MatrixRGBA;
MatrixRGBA.prototype.getRedNormal = function (x, y) {
    x = Math.floor((x%1 + 1)%1 * this.resolution);
    y = Math.floor((y%1 + 1)%1 * this.resolution);

    return this.buffer[4*(x + y*this.resolution)]/255;
};
MatrixRGBA.prototype.getGreenNormal = function (x, y) {
    x = Math.floor((x%1 + 1)%1 * this.resolution);
    y = Math.floor((y%1 + 1)%1 * this.resolution);

    return this.buffer[4*(x + y*this.resolution) + 1]/255;
};
MatrixRGBA.prototype.getBlueNormal = function (x, y) {
    x = Math.floor((x%1 + 1)%1 * this.resolution);
    y = Math.floor((y%1 + 1)%1 * this.resolution);

    return this.buffer[4*(x + y*this.resolution) + 2]/255;
};