function buildPerlin(resolution, canvas, seed) {
    let perlinScene = new THREE.Scene();

    let perlinCamera = new THREE.OrthographicCamera( -0.5, 0.5, 0.5, -0.5, 1, 1000 );
    perlinCamera.position.z = 1;

    let perlinRendeder = new THREE.WebGLRenderer({antialias:true, canvas: canvas});
    perlinRendeder.setClearColor("#000000");

    let perlinGeometry = new THREE.PlaneGeometry(1, 1, 1, 1);
    let perlinMaterial =  new THREE.ShaderMaterial({
        uniforms: {
            u_Resolution: {type: "vec2", value: new THREE.Vector2(resolution, resolution)},
            u_Scale: {type: "float", value: 10.0},
            u_Seed: {type: "float", value: seed || 47.0}
        },
        fragmentShader: document.getElementById("perlin-fragment").innerText,
        vertexShader: document.getElementById("simple-vertex").innerText
    });
    let perlinMesh = new THREE.Mesh(perlinGeometry, perlinMaterial);
    perlinScene.add( perlinMesh );
    if(canvas) {
        perlinRendeder.render(perlinScene, perlinCamera);
    }

    let renderTarget = new THREE.WebGLRenderTarget(resolution, resolution);
    perlinRendeder.setRenderTarget(renderTarget);
    perlinRendeder.render(perlinScene, perlinCamera);

    let buffer = new Uint8Array(resolution * resolution * 4);
    perlinRendeder.readRenderTargetPixels(renderTarget, 0, 0, resolution, resolution, buffer);

    return new MatrixRGBA(buffer, resolution);
}